<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HolaController extends Controller
{
    public function holamundo()
    {
        $saludo = '¿Qué tal va?';
        // $greetings = array('Hello', 'Bonjour', 'Bona sera');
        $greetings = [];
        return view('hola',[
            'saludo' => $saludo, 
            'greetings' => $greetings
            ]);
        // return view('directorio.hola');
        // hola.blade.php
        // hola.php
        // hola.html
    }
}
