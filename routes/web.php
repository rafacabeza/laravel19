<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hi', function () {
    return redirect(route('hola'));
});
Route::get('/hola', 'HolaController@holamundo')->name('hola');

Route::get('/hola/{name}', function ($name) {
    echo "Hola $name";
});

Route::get('books', 'BookController@index');




//get, post, put, patch, delete --> API RESTfull